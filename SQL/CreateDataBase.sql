CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE address(
	id_address uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
	cep VARCHAR(8),
	number_address int8,
	street VARCHAR (255),
	city VARCHAR (255),
  	uf VARCHAR (2),
  	created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);

CREATE TABLE employee(
	id_employee uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
	name VARCHAR (255) not null,
	email VARCHAR (255) not null,
  	cpf VARCHAR (11) not null,
  	phone VARCHAR (255) null,
  	birth_date TIMESTAMP not null,
  	created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT null,
  	id_address uuid not null,
  	FOREIGN KEY (id_address) REFERENCES address (id_address)
);

CREATE TABLE company(
	id_company uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
	name VARCHAR (255) not null,
  	cnpj VARCHAR (14) not null,
  	created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT null,
  	id_address uuid not null,
  	FOREIGN KEY (id_address)
      REFERENCES address (id_address)
);


CREATE TABLE company_employees_employee(
		"companyIdCompany" uuid not null,
		"employeeIdEmployee" uuid not null,
	PRIMARY KEY("companyIdCompany", "employeeIdEmployee"),
	FOREIGN KEY ("employeeIdEmployee")
      REFERENCES employee (id_employee) on delete cascade,
    FOREIGN KEY ("companyIdCompany")
      REFERENCES company (id_company) on delete cascade
);