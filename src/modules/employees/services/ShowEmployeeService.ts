import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import Employee from '../infra/typeorm/entities/Employee';
import IEmployeeRepository from '../repositories/IEmployeeRepository';

interface Request {
  id: string;
}

@injectable()
class ShowEmployeeService {
  constructor(
    @inject('EmployeeRepository')
    private employeeRepository: IEmployeeRepository,
  ) {}

  public async execute({ id }: Request): Promise<Employee> {
    const employee = await this.employeeRepository.findById(id);

    if (!employee) {
      throw new AppError('Employee not found');
    }

    return employee;
  }
}

export default ShowEmployeeService;
