import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import Employee from '../infra/typeorm/entities/Employee';
import IEmployeeRepository from '../repositories/IEmployeeRepository';
import IAddressRepository from '../../addresses/repositories/IAddressRepository';
import IUpdateEmployeeDTO from '../dtos/IUpdateEmployeeDTO';

@injectable()
class UpdateEmployeeService {
  constructor(
    @inject('EmployeeRepository')
    private employeeRepository: IEmployeeRepository,

    @inject('AddressRepository')
    private addressRepository: IAddressRepository,
  ) {}

  public async execute({
    id_employee,
    name,
    email,
    cpf,
    phone,
    birth_date,
    cep,
    city,
    number_address,
    street,
    uf,
  }: IUpdateEmployeeDTO): Promise<Employee> {
    const employee = await this.employeeRepository.findById(id_employee);

    if (!employee) {
      throw new AppError('Employee not found');
    }
    if (cpf) {
      const usedCpf = await this.employeeRepository.findByCpf(cpf);

      if (usedCpf && usedCpf.id_employee !== id_employee) {
        throw new AppError('CPF already used');
      }
    }

    const address = await this.addressRepository.findById(employee.id_address);
    if (!address) {
      throw new AppError('Address not found');
    }

    employee.name = name || employee.name;
    employee.email = email || employee.email;
    employee.cpf = cpf || employee.cpf;
    employee.phone = phone || employee.phone;
    employee.birth_date = birth_date || employee.birth_date;

    address.street = street || address.street;
    address.cep = cep || address.cep;
    address.city = city || address.city;
    address.number_address = number_address || address.number_address;
    address.uf = uf || address.uf;

    this.addressRepository.save(address);
    this.employeeRepository.save(employee);

    return employee;
  }
}

export default UpdateEmployeeService;
