import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import IEmployeeRepository from '../repositories/IEmployeeRepository';
import ICompanyRepository from '../../companies/repositories/ICompanyRepository';

import IRemoveCompanyDTO from '../dtos/IRemoveCompanyDTO';

@injectable()
class RemoveCompany {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,

    @inject('EmployeeRepository')
    private employeeRepository: IEmployeeRepository,
  ) {}

  public async execute({
    id_employee,
    cnpj,
  }: IRemoveCompanyDTO): Promise<void> {
    const employee = await this.employeeRepository.findById(id_employee);
    if (!employee) {
      throw new AppError('Employee not found');
    }
    const company = await this.companyRepository.findByCnpj(cnpj);
    if (!company) {
      throw new AppError('Company not found');
    }

    if (!employee.companies.length) {
      throw new AppError('Employee do not have company');
    }

    employee.companies.forEach(element => {
      if (element.id_company === company.id_company) {
        this.employeeRepository.removeCompany(element, employee);
      }
    });
  }
}
export default RemoveCompany;
