import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import IEmployeeRepository from '../repositories/IEmployeeRepository';

interface Request {
  id_employee: string;
}

@injectable()
class DeleteEmployeeService {
  constructor(
    @inject('EmployeeRepository')
    private employeeRepository: IEmployeeRepository,
  ) {}

  public async execute({ id_employee }: Request): Promise<void> {
    const employee = await this.employeeRepository.findById(id_employee);

    if (!employee) {
      throw new AppError('Employee not found');
    }

    this.employeeRepository.delete(id_employee);
  }
}

export default DeleteEmployeeService;
