import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import Employee from '../infra/typeorm/entities/Employee';
import IAddressRepository from '../../addresses/repositories/IAddressRepository';
import IEmployeeRepository from '../repositories/IEmployeeRepository';
import ICreateEmployeeDTO from '../dtos/ICreateEmployeeDTO';

@injectable()
class CreateEmployeeService {
  constructor(
    @inject('EmployeeRepository')
    private employeesRepository: IEmployeeRepository,

    @inject('AddressRepository')
    private addressRepository: IAddressRepository,
  ) {}

  public async execute({
    name,
    cpf,
    email,
    phone,
    birth_date,
    cep,
    city,
    number_address,
    street,
    uf,
  }: ICreateEmployeeDTO): Promise<Employee> {
    const usedCpf = await this.employeesRepository.findByCpf(cpf);
    if (usedCpf) {
      throw new AppError('CPF already used');
    }

    const { id_address } = await this.addressRepository.create({
      cep,
      city,
      number_address,
      street,
      uf,
    });

    const employee = await this.employeesRepository.create({
      name,
      cpf,
      email,
      phone,
      birth_date,
      id_address,
    });

    return employee;
  }
}

export default CreateEmployeeService;
