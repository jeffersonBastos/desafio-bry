import { injectable, inject } from 'tsyringe';
import IFindEmployeeDTO from '../dtos/IFindEmployeeDTO';

import Employee from '../infra/typeorm/entities/Employee';
import IEmployeeRepository from '../repositories/IEmployeeRepository';

@injectable()
class ListEmployeeService {
  constructor(
    @inject('EmployeeRepository')
    private employeeRepository: IEmployeeRepository,
  ) {}

  public async execute(filter?: IFindEmployeeDTO): Promise<Employee[]> {
    const employee = await this.employeeRepository.findAll(filter);

    return employee;
  }
}

export default ListEmployeeService;
