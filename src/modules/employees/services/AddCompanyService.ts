import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import ICompanyRepository from '../../companies/repositories/ICompanyRepository';
import IEmployeeRepository from '../repositories/IEmployeeRepository';

import IAddEmployeeDTO from '../dtos/IAddcCompanyDTO';
import Employee from '../infra/typeorm/entities/Employee';

@injectable()
class AddCompany {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,

    @inject('EmployeeRepository')
    private employeeRepository: IEmployeeRepository,
  ) {}

  public async execute({
    id_employee,
    cnpj,
  }: IAddEmployeeDTO): Promise<Employee> {
    const employee = await this.employeeRepository.findById(id_employee);
    if (!employee) {
      throw new AppError('Employee not found');
    }
    const company = await this.companyRepository.findByCnpj(cnpj);
    if (!company) {
      throw new AppError('Company not found');
    }

    if (employee.companies.length) {
      employee.companies.forEach(element => {
        if (element.id_company === company.id_company) {
          throw new AppError('Company already inserted');
        }
      });
    }

    employee.companies = [...employee.companies, company];
    this.employeeRepository.save(employee);

    return employee;
  }
}
export default AddCompany;
