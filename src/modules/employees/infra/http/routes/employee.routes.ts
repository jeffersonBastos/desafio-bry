import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import EmployeeController from '../controllers/EmployeeController';

const employeeRouter = Router();
const employeeController = new EmployeeController();

employeeRouter.get('/', employeeController.list);
employeeRouter.get('/:id_employee', employeeController.show);
employeeRouter.delete('/:id_employee', employeeController.delete);

employeeRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      cpf: Joi.string().required(),
      email: Joi.string().required(),
      phone: Joi.string().required(),
      birth_date: Joi.date().required(),
      number_address: Joi.number().required(),
      cep: Joi.string().required(),
      street: Joi.string().required(),
      city: Joi.string().required(),
      uf: Joi.string().min(2).max(2).required(),
    },
  }),
  employeeController.create,
);

employeeRouter.put(
  '/:id_employee',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      cpf: Joi.string().min(11).max(11).required(),
      email: Joi.string().required(),
      phone: Joi.string().required(),
      birth_date: Joi.date().required(),
      number_address: Joi.number().required(),
      cep: Joi.string().required(),
      street: Joi.string().required(),
      city: Joi.string().required(),
      uf: Joi.string().min(2).max(2).required(),
    },
  }),
  employeeController.update,
);
employeeRouter.delete(
  '/removeCompany/:id_employee',
  celebrate({
    [Segments.BODY]: {
      cnpj: Joi.string().required(),
    },
  }),
  employeeController.removeCompany,
);
employeeRouter.put(
  '/addCompany/:id_employee',
  celebrate({
    [Segments.BODY]: {
      cnpj: Joi.string().required(),
    },
  }),
  employeeController.addCompany,
);

export default employeeRouter;
