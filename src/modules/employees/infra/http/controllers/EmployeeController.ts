import { Request, Response } from 'express';
import { container } from 'tsyringe';

import UpdateEmployeeService from '../../../services/UpdateEmployeeService';
import CreateEmployeeService from '../../../services/CreateEmployeeService';
import ListEmployeeService from '../../../services/ListEmployeeService';
import ShowEmployeeService from '../../../services/ShowEmployeeService';
import DeleteEmployeeService from '../../../services/DeleteEmployeeService';
import AddEmployeeService from '../../../services/AddCompanyService';
import removeCompanyService from '../../../services/RemoveCompanyService';

export default class EmployeeController {
  public async list(request: Request, response: Response): Promise<Response> {
    const { filter = '' } = request.query;

    const listEmployee = container.resolve(ListEmployeeService);

    const employee = await listEmployee.execute({
      filter: String(filter),
    });

    return response.json(employee);
  }

  public async show(request: Request, response: Response): Promise<Response> {
    const { id_employee } = request.params;

    const showEmployee = container.resolve(ShowEmployeeService);

    const employee = await showEmployee.execute({
      id: id_employee,
    });

    return response.json(employee);
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const {
      name,
      cpf,
      birth_date,
      email,
      phone,
      cep,
      city,
      number_address,
      street,
      uf,
    } = request.body;

    const createEmployee = container.resolve(CreateEmployeeService);

    const employee = await createEmployee.execute({
      name,
      cpf,
      birth_date,
      email,
      phone,
      cep,
      city,
      number_address,
      street,
      uf,
    });

    return response.json(employee);
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const { id_employee } = request.params;
    const {
      name,
      cpf,
      birth_date,
      email,
      phone,
      cep,
      city,
      number_address,
      street,
      uf,
    } = request.body;

    const updateEmployee = container.resolve(UpdateEmployeeService);

    const employee = await updateEmployee.execute({
      id_employee,
      name,
      cpf,
      birth_date,
      email,
      phone,
      cep,
      city,
      number_address,
      street,
      uf,
    });

    return response.json(employee);
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const { id_employee } = request.params;

    const deleteEmployee = container.resolve(DeleteEmployeeService);

    await deleteEmployee.execute({
      id_employee,
    });

    return response.status(204).send();
  }

  public async removeCompany(
    request: Request,
    response: Response,
  ): Promise<Response> {
    const { id_employee } = request.params;
    const { cnpj } = request.body;

    const removeCompany = container.resolve(removeCompanyService);

    await removeCompany.execute({
      id_employee,
      cnpj,
    });

    return response.status(204).send();
  }

  public async addCompany(
    request: Request,
    response: Response,
  ): Promise<Response> {
    const { id_employee } = request.params;
    const { cnpj } = request.body;

    const addCompany = container.resolve(AddEmployeeService);

    const employee = await addCompany.execute({
      id_employee,
      cnpj,
    });

    return response.json(employee);
  }
}
