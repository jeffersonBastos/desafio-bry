import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import Company from '../../../../companies/infra/typeorm/entities/Company';

import Address from '../../../../addresses/infra/typeorm/entities/Address';

@Entity('employee')
class Employee {
  @PrimaryGeneratedColumn('uuid')
  id_employee: string;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  cpf: string;

  @Column()
  phone: string;

  @Column()
  birth_date: Date;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  id_address: string;

  @ManyToOne(() => Address)
  @JoinColumn({ name: 'id_address' })
  address: Address;

  @ManyToMany(() => Company, company => company.employees)
  companies: Company[];
}
export default Employee;
