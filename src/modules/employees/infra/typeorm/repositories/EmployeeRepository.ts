import { getRepository, Repository, Raw } from 'typeorm';
import Company from 'modules/companies/infra/typeorm/entities/Company';
import Employee from '../entities/Employee';
import IEmployeeRepository from '../../../repositories/IEmployeeRepository';
import IFindEmployeeDTO from '../../../dtos/IFindEmployeeDTO';

interface CreateData {
  name: string;
  cpf: string;
  email: string;
  phone: string;
  birth_date: Date;
  id_address: string;
}

class EmployeeRepository implements IEmployeeRepository {
  private ormRepository: Repository<Employee>;

  constructor() {
    this.ormRepository = getRepository(Employee);
  }

  public async findById(id: string): Promise<Employee | undefined> {
    const employee = await this.ormRepository.findOne({
      where: {
        id_employee: id,
      },
      relations: ['address', 'companies'],
    });

    return employee;
  }

  public async findAll({ filter }: IFindEmployeeDTO): Promise<Employee[]> {
    const employee = await this.ormRepository.find({
      where: [
        { name: Raw(name => `${name} ILIKE '%${filter}%'`) },
        { cpf: Raw(cpf => `${cpf} ILIKE '%${filter}%'`) },
      ],
      relations: ['address', 'companies'],
    });
    return employee;
  }

  public async create({
    name,
    cpf,
    email,
    phone,
    birth_date,
    id_address,
  }: CreateData): Promise<Employee> {
    const employee = this.ormRepository.create({
      name,
      cpf,
      email,
      phone,
      birth_date,
      id_address,
    });

    await this.ormRepository.save(employee);

    return employee;
  }

  public async save(employee: Employee): Promise<Employee> {
    return this.ormRepository.save(employee);
  }

  public async delete(id: string): Promise<void> {
    this.ormRepository.delete(id);
  }

  public async findByCpf(cpf: string): Promise<Employee | undefined> {
    const employee = await this.ormRepository.findOne({
      where: { cpf },
    });

    return employee;
  }

  public async removeCompany(
    company: Company,
    employee: Employee,
  ): Promise<void> {
    employee.companies.forEach((element, index) => {
      if (element.id_company === company.id_company) {
        employee.companies.splice(index, 1);
      }
    });
    await this.ormRepository.save(employee);
  }
}
export default EmployeeRepository;
