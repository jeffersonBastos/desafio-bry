export default interface IAddCompanyDTO {
  id_employee: string;
  cnpj: string;
}
