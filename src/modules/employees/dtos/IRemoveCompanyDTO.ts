export default interface IRemoveCompanyDTO {
  id_employee: string;
  cnpj: string;
}
