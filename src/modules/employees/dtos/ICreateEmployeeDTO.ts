export default interface ICreateEmployeeDTO {
  name: string;
  cpf: string;
  email: string;
  phone: string;
  birth_date: Date;
  number_address: number;
  cep: string;
  street: string;
  city: string;
  uf: string;
}
