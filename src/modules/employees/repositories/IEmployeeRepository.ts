import Employee from '../infra/typeorm/entities/Employee';
import IFindEmployeeDTO from '../dtos/IFindEmployeeDTO';
import Company from '../../companies/infra/typeorm/entities/Company';

interface CreateData {
  name: string;
  cpf: string;
  email: string;
  phone: string;
  birth_date: Date;
  id_address: string;
}

export default interface IEmployeeRepository {
  findById(id: string): Promise<Employee | undefined>;
  findAll(filter?: IFindEmployeeDTO): Promise<Employee[]>;
  create(data: CreateData): Promise<Employee>;
  save(employee: Employee): Promise<Employee>;
  delete(id: string): Promise<void>;
  findByCpf(cpf: string): Promise<Employee | undefined>;
  removeCompany(company: Company, employee: Employee): Promise<void>;
}
