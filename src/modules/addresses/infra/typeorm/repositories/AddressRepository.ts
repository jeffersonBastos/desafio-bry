import { getRepository, Repository } from 'typeorm';
import Address from '../entities/Address';

import IAddressRepository from '../../../repositories/IAddressRepository';
import ICreateAddressDTO from '../../../dtos/ICreateAddressDTO';

class AddressRepository implements IAddressRepository {
  private ormRepository: Repository<Address>;

  constructor() {
    this.ormRepository = getRepository(Address);
  }

  public async findById(id: string): Promise<Address | undefined> {
    const address = await this.ormRepository.findOne(id);

    return address;
  }

  public async create({
    cep,
    city,
    number_address,
    street,
    uf,
  }: ICreateAddressDTO): Promise<Address> {
    const address = this.ormRepository.create({
      cep,
      city,
      number_address,
      street,
      uf,
    });
    await this.ormRepository.save(address);

    return address;
  }

  public async save(address: Address): Promise<Address> {
    return this.ormRepository.save(address);
  }

  public async delete(id: string): Promise<void> {
    this.ormRepository.delete(id);
  }
}
export default AddressRepository;
