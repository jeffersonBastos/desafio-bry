export default interface IUpdateAddressDTO {
  number_adrres?: number;
  cep?: string;
  street?: string;
  city?: string;
  uf?: string;
}
