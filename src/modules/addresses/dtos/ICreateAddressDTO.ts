export default interface ICreateAddressDTO {
  number_address: number;
  cep: string;
  street: string;
  city: string;
  uf: string;
}
