import Company from '../infra/typeorm/entities/Company';
import IFindCompanyDTO from '../dtos/IFindCompanyDTO';
import Employee from '../../employees/infra/typeorm/entities/Employee';

interface CreateData {
  name: string;
  cnpj: string;
  id_address: string;
}
export default interface ICompanyRepository {
  findById(id: string): Promise<Company | undefined>;
  findAll(filter?: IFindCompanyDTO): Promise<Company[]>;
  create(data: CreateData): Promise<Company>;
  save(company: Company): Promise<Company>;
  delete(id: string): Promise<void>;
  findByCnpj(cpnj: string): Promise<Company | undefined>;
  removeEmployee(employee: Employee, company: Company): Promise<void>;
}
