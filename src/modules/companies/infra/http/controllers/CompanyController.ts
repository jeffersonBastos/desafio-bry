import { Request, Response } from 'express';
import { container } from 'tsyringe';

import UpdateCompanyService from '../../../services/UpdateCompanyService';
import CreateCompanyService from '../../../services/CreateCompanyService';
import ListCompanyService from '../../../services/ListCompanyService';
import ShowCompanyService from '../../../services/ShowCompanyService';
import DeleteCompanyService from '../../../services/DeleteCompanyService';
import AddEmployeeService from '../../../services/AddEmployeeService';
import RemoveEmployeeService from '../../../services/RemoveEmployeeService';

export default class CompanyController {
  public async list(request: Request, response: Response): Promise<Response> {
    const { filter = '' } = request.query;

    const listCompany = container.resolve(ListCompanyService);

    const company = await listCompany.execute({
      filter: String(filter),
    });

    return response.json(company);
  }

  public async show(request: Request, response: Response): Promise<Response> {
    const { id_company } = request.params;

    const showCompany = container.resolve(ShowCompanyService);

    const company = await showCompany.execute({
      id: id_company,
    });

    return response.json({ company });
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const { name, cnpj, cep, city, number_address, street, uf } = request.body;

    const createCompany = container.resolve(CreateCompanyService);

    const company = await createCompany.execute({
      name,
      cnpj,
      cep,
      city,
      number_address,
      street,
      uf,
    });

    return response.json(company);
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const { id_company } = request.params;
    const { name, cnpj, cep, city, number_address, street, uf } = request.body;

    const updateCompany = container.resolve(UpdateCompanyService);

    const company = await updateCompany.execute({
      id_company,
      name,
      cnpj,
      cep,
      city,
      number_address,
      street,
      uf,
    });

    return response.json(company);
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const { id_company } = request.params;

    const deleteCompany = container.resolve(DeleteCompanyService);

    await deleteCompany.execute({
      id_company,
    });

    return response.status(204).send();
  }

  public async removeEmployee(
    request: Request,
    response: Response,
  ): Promise<Response> {
    const { id_company } = request.params;
    const { cpf } = request.body;

    const removeEmployee = container.resolve(RemoveEmployeeService);

    await removeEmployee.execute({
      id_company,
      cpf,
    });

    return response.status(204).send();
  }

  public async addEmployee(
    request: Request,
    response: Response,
  ): Promise<Response> {
    const { id_company } = request.params;
    const { cpf } = request.body;

    const addEmployee = container.resolve(AddEmployeeService);

    const company = await addEmployee.execute({
      id_company,
      cpf,
    });

    return response.json(company);
  }
}
