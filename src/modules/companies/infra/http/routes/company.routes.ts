import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';

import CompanyController from '../controllers/CompanyController';

const companyRouter = Router();
const companyController = new CompanyController();

companyRouter.get('/', companyController.list);
companyRouter.get('/:id_company', companyController.show);
companyRouter.delete('/:id_company', companyController.delete);

companyRouter.post(
  '/',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      cnpj: Joi.string().required(),
      number_address: Joi.number().required(),
      cep: Joi.string().required(),
      street: Joi.string().required(),
      city: Joi.string().required(),
      uf: Joi.string().min(2).max(2).required(),
    },
  }),
  companyController.create,
);
companyRouter.put(
  '/:id_company',
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required(),
      cnpj: Joi.string().required(),
      number_address: Joi.number().required(),
      cep: Joi.string().required(),
      street: Joi.string().required(),
      city: Joi.string().required(),
      uf: Joi.string().min(2).max(2).required(),
    },
  }),
  companyController.update,
);
companyRouter.delete(
  '/removeEmployee/:id_company',
  celebrate({
    [Segments.BODY]: {
      cpf: Joi.string().required(),
    },
  }),
  companyController.removeEmployee,
);
companyRouter.put(
  '/addEmployee/:id_company',
  celebrate({
    [Segments.BODY]: {
      cpf: Joi.string().required(),
    },
  }),
  companyController.addEmployee,
);

export default companyRouter;
