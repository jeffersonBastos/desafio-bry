import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  ManyToOne,
  JoinColumn,
  JoinTable,
} from 'typeorm';

import Employee from '../../../../employees/infra/typeorm/entities/Employee';
import Address from '../../../../addresses/infra/typeorm/entities/Address';

@Entity('company')
class Company {
  @PrimaryGeneratedColumn('uuid')
  id_company: string;

  @Column()
  name: string;

  @Column()
  cnpj: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @Column()
  id_address: string;

  @ManyToOne(() => Address)
  @JoinColumn({ name: 'id_address' })
  address: Address;

  @ManyToMany(() => Employee, employee => employee.companies)
  @JoinTable()
  employees: Employee[];
}
export default Company;
