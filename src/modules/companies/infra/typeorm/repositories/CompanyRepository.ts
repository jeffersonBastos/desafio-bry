import { getRepository, Repository, Raw } from 'typeorm';
import Employee from 'modules/employees/infra/typeorm/entities/Employee';
import Company from '../entities/Company';

import ICompanyRepository from '../../../repositories/ICompanyRepository';
import IFindCompanyDTO from '../../../dtos/IFindCompanyDTO';

interface CreateData {
  name: string;
  cnpj: string;
  id_address: string;
}

class CompanyRepository implements ICompanyRepository {
  private ormRepository: Repository<Company>;

  constructor() {
    this.ormRepository = getRepository(Company);
  }

  public async findById(id: string): Promise<Company | undefined> {
    const company = await this.ormRepository.findOne({
      where: {
        id_company: id,
      },
      relations: ['address', 'employees'],
    });

    return company;
  }

  public async findAll({ filter }: IFindCompanyDTO): Promise<Company[]> {
    const company = await this.ormRepository.find({
      where: [
        { name: Raw(name => `${name} ILIKE '%${filter}%'`) },
        { cnpj: Raw(cnpj => `${cnpj} ILIKE '%${filter}%'`) },
      ],
      relations: ['address', 'employees'],
    });
    return company;
  }

  public async create({
    name,
    cnpj,
    id_address,
  }: CreateData): Promise<Company> {
    const company = this.ormRepository.create({
      name,
      cnpj,
      id_address,
    });
    await this.ormRepository.save(company);

    return company;
  }

  public async save(company: Company): Promise<Company> {
    return this.ormRepository.save(company);
  }

  public async delete(id: string): Promise<void> {
    this.ormRepository.delete(id);
  }

  public async findByCnpj(cnpj: string): Promise<Company | undefined> {
    const company = await this.ormRepository.findOne({
      where: { cnpj },
    });

    return company;
  }

  public async removeEmployee(
    employee: Employee,
    company: Company,
  ): Promise<void> {
    company.employees.forEach((element, index) => {
      if (element.id_employee === employee.id_employee) {
        company.employees.splice(index, 1);
      }
    });
    await this.ormRepository.save(company);
  }
}
export default CompanyRepository;
