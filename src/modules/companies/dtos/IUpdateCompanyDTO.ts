export default interface IUpdateCompanyDTO {
  id_company: string;
  name?: string;
  cnpj?: string;
  number_address?: number;
  cep?: string;
  street?: string;
  city?: string;
  uf?: string;
}
