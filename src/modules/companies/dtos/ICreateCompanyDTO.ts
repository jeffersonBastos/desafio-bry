export default interface ICreateCompanyDTO {
  name: string;
  cnpj: string;
  number_address: number;
  cep: string;
  street: string;
  city: string;
  uf: string;
}
