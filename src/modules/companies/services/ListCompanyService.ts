import { injectable, inject } from 'tsyringe';
import IFindCompanyDTO from '../dtos/IFindCompanyDTO';

import Company from '../infra/typeorm/entities/Company';
import ICompanyRepository from '../repositories/ICompanyRepository';

@injectable()
class ListCompanyService {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,
  ) {}

  public async execute(filter?: IFindCompanyDTO): Promise<Company[]> {
    const company = await this.companyRepository.findAll(filter);

    return company;
  }
}

export default ListCompanyService;
