import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import ICompanyRepository from '../repositories/ICompanyRepository';
import IEmployeeRepository from '../../employees/repositories/IEmployeeRepository';

import IRemoveEmployeeDTO from '../dtos/IRemoveEmployeeDTO';

@injectable()
class RemoveEmployee {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,

    @inject('EmployeeRepository')
    private employeeRepository: IEmployeeRepository,
  ) {}

  public async execute({ id_company, cpf }: IRemoveEmployeeDTO): Promise<void> {
    const company = await this.companyRepository.findById(id_company);
    if (!company) {
      throw new AppError('Company not found');
    }
    const employee = await this.employeeRepository.findByCpf(cpf);
    if (!employee) {
      throw new AppError('Employee not found');
    }

    if (!company.employees.length) {
      throw new AppError('Company do not have employees');
    }

    company.employees.forEach(element => {
      if (element.id_employee === employee.id_employee) {
        this.companyRepository.removeEmployee(element, company);
      }
    });
  }
}
export default RemoveEmployee;
