import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import Company from '../infra/typeorm/entities/Company';
import ICompanyRepository from '../repositories/ICompanyRepository';
import IUpdateCompanyDTO from '../dtos/IUpdateCompanyDTO';
import IAddressRepository from '../../addresses/repositories/IAddressRepository';

@injectable()
class UpdateCompanyService {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,

    @inject('AddressRepository')
    private addressRepository: IAddressRepository,
  ) {}

  public async execute({
    id_company,
    name,
    cnpj,
    cep,
    city,
    number_address,
    street,
    uf,
  }: IUpdateCompanyDTO): Promise<Company> {
    const company = await this.companyRepository.findById(id_company);

    if (!company) {
      throw new AppError('Company not found');
    }

    if (cnpj) {
      const usedCnpj = await this.companyRepository.findByCnpj(cnpj);

      if (usedCnpj && usedCnpj.id_company !== id_company) {
        throw new AppError('CNPJ already used');
      }
    }

    const address = await this.addressRepository.findById(company.id_address);

    if (!address) {
      throw new AppError('Address not found');
    }

    company.name = name || company.name;
    company.cnpj = cnpj || company.cnpj;

    address.street = street || address.street;
    address.cep = cep || address.cep;
    address.city = city || address.city;
    address.number_address = number_address || address.number_address;
    address.uf = uf || address.uf;

    this.addressRepository.save(address);
    this.companyRepository.save(company);

    return company;
  }
}

export default UpdateCompanyService;
