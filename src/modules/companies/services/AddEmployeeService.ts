import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import ICompanyRepository from '../repositories/ICompanyRepository';
import IEmployeeRepository from '../../employees/repositories/IEmployeeRepository';

import IAddEmployeeDTO from '../dtos/IAddEmployeeDTO';
import Company from '../infra/typeorm/entities/Company';

@injectable()
class AddEmployee {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,

    @inject('EmployeeRepository')
    private employeeRepository: IEmployeeRepository,
  ) {}

  public async execute({ id_company, cpf }: IAddEmployeeDTO): Promise<Company> {
    const company = await this.companyRepository.findById(id_company);
    if (!company) {
      throw new AppError('Company not found');
    }
    const employee = await this.employeeRepository.findByCpf(cpf);

    if (!employee) {
      throw new AppError('Employee not found');
    }

    if (company.employees.length) {
      company.employees.forEach(element => {
        if (element.id_employee === employee.id_employee) {
          throw new AppError('Employee already inserted');
        }
      });
    }

    company.employees = [...company.employees, employee];

    this.companyRepository.save(company);

    return company;
  }
}
export default AddEmployee;
