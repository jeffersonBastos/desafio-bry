import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import Company from '../infra/typeorm/entities/Company';
import ICompanyRepository from '../repositories/ICompanyRepository';
import IAddressRepository from '../../addresses/repositories/IAddressRepository';
import ICreateCompanyDTO from '../dtos/ICreateCompanyDTO';

@injectable()
class CreateCompanyService {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,

    @inject('AddressRepository')
    private addressRepository: IAddressRepository,
  ) {}

  public async execute({
    name,
    cnpj,
    cep,
    city,
    number_address,
    street,
    uf,
  }: ICreateCompanyDTO): Promise<Company> {
    const usedCnpj = await this.companyRepository.findByCnpj(cnpj);
    if (usedCnpj) {
      throw new AppError('CNPJ already used');
    }
    const { id_address } = await this.addressRepository.create({
      cep,
      city,
      number_address,
      street,
      uf,
    });

    const company = await this.companyRepository.create({
      name,
      cnpj,
      id_address,
    });

    return company;
  }
}

export default CreateCompanyService;
