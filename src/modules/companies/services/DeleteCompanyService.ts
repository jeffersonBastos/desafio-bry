import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import ICompanyRepository from '../repositories/ICompanyRepository';

interface Request {
  id_company: string;
}

@injectable()
class DeleteCompanyService {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,
  ) {}

  public async execute({ id_company }: Request): Promise<void> {
    const company = await this.companyRepository.findById(id_company);

    if (!company) {
      throw new AppError('Company not found');
    }

    this.companyRepository.delete(id_company);
  }
}

export default DeleteCompanyService;
