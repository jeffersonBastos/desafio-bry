import { injectable, inject } from 'tsyringe';
import AppError from '../../../shared/errors/AppError';

import Company from '../infra/typeorm/entities/Company';
import ICompanyRepository from '../repositories/ICompanyRepository';

interface Request {
  id: string;
}

@injectable()
class ShowCompanyService {
  constructor(
    @inject('CompanyRepository')
    private companyRepository: ICompanyRepository,
  ) {}

  public async execute({ id }: Request): Promise<Company> {
    const company = await this.companyRepository.findById(id);

    if (!company) {
      throw new AppError('Company not found');
    }

    return company;
  }
}

export default ShowCompanyService;
