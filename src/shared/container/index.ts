import { container } from 'tsyringe';

import IEmployeeRepository from '../../modules/employees/repositories/IEmployeeRepository';
import EmployeeRepository from '../../modules/employees/infra/typeorm/repositories/EmployeeRepository';

import ICompanyRepository from '../../modules/companies/repositories/ICompanyRepository';
import CompanyRepository from '../../modules/companies/infra/typeorm/repositories/CompanyRepository';

import IAddressRepository from '../../modules/addresses/repositories/IAddressRepository';
import AddressRepository from '../../modules/addresses/infra/typeorm/repositories/AddressRepository';

container.registerSingleton<IEmployeeRepository>(
  'EmployeeRepository',
  EmployeeRepository,
);

container.registerSingleton<ICompanyRepository>(
  'CompanyRepository',
  CompanyRepository,
);

container.registerSingleton<IAddressRepository>(
  'AddressRepository',
  AddressRepository,
);
