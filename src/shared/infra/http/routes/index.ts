import { Router } from 'express';

import CompanyRouter from '../../../../modules/companies/infra/http/routes/company.routes';
import EmployeeRouter from '../../../../modules/employees/infra/http/routes/employee.routes';

const routes = Router();

routes.use('/company', CompanyRouter);
routes.use('/employee', EmployeeRouter);

export default routes;
