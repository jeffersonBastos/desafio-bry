# Desafio Bry
## Banco de Dados

Foi usado o banco de dados PostgreSQL. 
Na pasta SQL é possível encontrar o arquivo de criação do banco.
É necessário configurar o arquivo ormconfig.json localizado na raiz do projeto de acordo com as configurações de conexão do banco.

## Instalação

Depois de subir o banco de dados e criar as tabelas necessárias. É preciso baixar todas as dependências com o comando:

```bash
npm install
```
E para executar a aplicação:

```bash
yarn dev 
```
ou
```bash
npm run dev
```
## Ferramentas

Foi usada a ferramenta ESLint para análise de código estática no intuito de identificar padrões problemáticos. E como formatador de código foi usado o Prettier.

## Rotas

#### COMPANY:
**GET** http://localhost:3333/company

Lista todas as “companies”

**GET**	 http://localhost:3333/company/Id_company

Busca uma company pelo id

**POST**	 http://localhost:3333/company/

Cria uma company.
 ***Body:***

```json
 {
 "name": "Jefferson",
  "cnpj": "33308895735999",
  "number_address":  "225",
  "cep":  "8805010",
  "street":  "serv natalina machado",
  "city":  "Florianopolis",
  "uf": "SC"
}
```
**PUT**	 http://localhost:3333/company/Id_company

Faz a atualização de uma company.
**Body** completo: 
```json
 {
    "name": "Jose",
    "cnpj": "12345678910111",
    "number_address":  "100",
    "cep":  "8805010",
    "street":  "serv natalina machado",
    "city":  "Florianopolis",
    "uf": "SC"
}
--Os parâmetros são opcionais
```
**DELETE**	 http://localhost:3333/company/Id_company

Deleta uma company


**PUT**	 http://localhost:3333/company/Id_company

Adiciona um employee em uma company passando o CPF.

**DELETE**	http://localhost:3333/company/Id_company

Remove um employee de uma company pelo CPF.
```json
{
    "cpf": "08833333300"
}
```

#### EMPLOYEE:

**GET** http://localhost:3333/employee

Lista todos os employees

**GET**	 http://localhost:3333/employee/Id_employee

Busca um employee pelo id

**POST**	 http://localhost:3333/employee/

Cria um employee.
 ***Body:***

```json
 {
 "name": "Bry",
  "cpf": "08833333300",
  "email":  "jeff.junior98@gmail.com",
  "phone":  "48984231025",
  "birth_date":  "12-05-2021",
  "number_address":  "225",
  "cep":  "8805010",
  "street":  "serv natalina machado",
  "city":  "Florianopolis",
  "uf": "SC"
}

```
**PUT**	 http://localhost:3333/employee/Id_employee

Faz a atualização de um employee.
**Body** completo: 
```json
{
 "name": "Marcio",
  "cpf": "08895790099",
  "email":  "jeff98@gmail.com",
  "phone":  "48984231025",
  "birth_date":  "12-05-2021",
  "number_address":  "115",
  "cep":  "1234567",
  "street":  "serv machado",
  "city":  "Curitiba",
  "uf": "PR"
}
--Os parâmetros são opcionais
```
**DELETE**	 http://localhost:3333/employee/Id_employee

Deleta um employee

**PUT**	 http://localhost:3333/employee/Id_employee

Adiciona uma company  em um employee passando o CNPJ.

**DELETE**	http://localhost:3333/employee/Id_employee

Remove uma company de um employee pelo CNPJ.
```json
{
    "cnpj": "33308895735900"
}
```

# Perguntas:

1-Eu usaria o SDK em JAVA, pois já está pronto para uso. Existem diversos tipos de QRCode, necessário para a confecção do mesmo, saber o tipo (imagem, URL, email, texto...) e dependendo do tipo do QRCode as entradas mudariam. E como saída retornaria uma imagem PNG do QRCode.

2-Teoricamente não deveria ter impacto no serviço do cliente. Porém, para diminuir o mesmo, o ideal seria ter servidores diferentes, com ambiente de homologação e de produção. Assim, em um horário de baixo ou sem uso do serviço, efetuar a “troca” total do serviço. O impacto que isso geraria ao cliente seria o serviço indisponível durante a troca.
